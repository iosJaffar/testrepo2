//
//  ViewController.swift
//  googleMap
//
//  Created by Greens6 on 01/01/21.
//  Copyright © 2021 greenstech. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let camera = GMSCameraPosition.camera(withLatitude: 13.0012, longitude: 80.2565, zoom: 10.0)
        let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        self.view.addSubview(mapView)

        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 13.0012, longitude: 80.2565)
        marker.title = "Adyar"
        marker.snippet = "Chennai"
        marker.map = mapView
    }


}

